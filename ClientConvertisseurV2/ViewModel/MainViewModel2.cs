﻿using ClientConvertisseurV2.Models;
using ClientConvertisseurV2.Service;
using ClientConvertisseurV2.View;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace ClientConvertisseurV2.ViewModel
{
    public class MainViewModel2 : ViewModelBase
    {
        private string _montantEuros;
        private string _resultatConversion;
        private Devise _comboBoxDeviseItem;
        private ObservableCollection<Devise> _comboBoxDevises;
        public ObservableCollection<Devise> ComboBoxDevises
        {
            get { return _comboBoxDevises; }
            set
            {
                _comboBoxDevises = value;
                RaisePropertyChanged();
            }
        }
        public ICommand BtnSetConversion { get; private set; }

        public MainViewModel2()
        {
            ActionGetData();
            BtnSetConversion = new RelayCommand(ActionSetConversion);
        }
        private async void ActionGetData()
        {
            var result = await WSService.Instance.GetAllDevisesAsync();
            this.ComboBoxDevises = new ObservableCollection<Devise>(result);
        }
        public string MontantEuros
        {
            get
            {
                return _montantEuros;
            }
            set
            {
                _montantEuros = value;
                RaisePropertyChanged();
            }
        }
        public Devise ComboBoxDeviseItem
        {
            get
            {
                return _comboBoxDeviseItem;
            }
            set
            {
                _comboBoxDeviseItem = value;
                RaisePropertyChanged();
            }
        }

        public string ResultatConversion
        {
            get
            {
                return _resultatConversion;
            }
            set
            {
                _resultatConversion = value;
                RaisePropertyChanged();
            }
        }

        private void ActionSetConversion()
        {
            string montant = _resultatConversion;
            Devise devise = _comboBoxDeviseItem;
            double conversion = Convertir(montant, devise);
            MontantEuros = conversion.ToString();
        }

        private double Convertir(string montant, Devise devise)
        {
            double resultat = 0;
            if (!double.TryParse(montant, out resultat))
            {
                MessageErreur();
            }

            if (devise == null)
            {
                MessageErreur();
            }

            return resultat / devise.Taux;
        }

        private async void MessageErreur()
        {
            Windows.UI.Popups.MessageDialog message = new MessageDialog("Montant non valide !");
            await message.ShowAsync();

        }
    }
}
